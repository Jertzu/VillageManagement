import random

import tdl
from classes import Villager
from classes import Resource
from events import EventManager

class Game():
	def __init__(self):
		self.width = 120
		self.height = 60
		self.frameLimit = 60
		self.running = True
		tdl.set_font('terminal8x12.png', greyscale=True, altLayout=True)
		self.console = tdl.init(self.width, self.height, "Village Management", False)
		self.console.set_colors((170, 170, 80))
		tdl.setFPS(self.frameLimit)

		self.villagers = []
		for i in range(random.randint(10, 20)):
			self.villagers.append(Villager())

		self.year = 1562
		self.season = "Earth"

		self.food = Resource("Food", len(self.villagers) * 6)

		self.firstRun = True

		self.eventManager = EventManager(self)
		self.eventManager.run_event(self.eventManager.event_beginning)

		self.pendingActions = "next_event"

	def run(self):
		while self.running:
			if tdl.event.is_window_closed():
				self.running = False
			self.update()

	def update(self):
		if self.firstRun:
			self.update_logic()
			self.draw_all()
			self.firstRun = False
		self.handle_input()
		self.update_logic()
		self.draw_all()

	def handle_input(self):
		input = tdl.event.key_wait()

		if input.key == "ESCAPE":
			self.running = False

		if not self.eventManager.currentEvent:
			if input.key == "ENTER" and self.pendingActions == None:
				self.pendingActions = "next_season"
		else:
			self.eventManager.handle_input(input)

	def update_logic(self):
		if self.pendingActions == "next_season":
			self.next_season()

		if self.pendingActions == "next_event":
			if self.eventManager.eventList:
				self.eventManager.eventList[0]()
				self.eventManager.eventList.pop(0)

		if not self.eventManager.currentEvent:
			self.pendingActions = None
			self.season_colors()

	def draw_all(self):
		self.console.clear()

		self.console.draw_frame(1, 1, self.width - 2, self.height - 2, "#")

		if self.eventManager.currentEvent:
			self.eventManager.currentEvent.draw()
		else:
			self.console.draw_str(3, 3, "Village - Calendar: " + self.season + " season of the year " + str(self.year))
			self.console.draw_str(3, 5, "Adults: " + str(self.get_adults()) + " (" + str(self.get_adults("male")) + "M/" + str(self.get_adults("female")) + "F)")
			self.console.draw_str(3, 6, "Children: " + str(self.get_children()))
			self.console.draw_str(3, 8, "Food: " + str(self.food.amount))
			self.console.draw_str(3, self.height - 4, "[ENTER] Next season.")

		tdl.flush()

	def season_colors(self):
		if self.season == "Earth":
			self.console.set_colors((170, 170, 80))
		elif self.season == "Fire":
			self.console.set_colors((120, 190, 80))
		elif self.season == "Water":
			self.console.set_colors((170, 120, 80))
		elif self.season == "Wind":
			self.console.set_colors((120, 120, 160))

	def next_season(self):
		self.update_pregnancy()
		self.feed_villagers()

		self.random_event()

		if self.season == "Earth":
			self.season = "Fire"
		elif self.season == "Fire":
			self.season = "Water"
		elif self.season == "Water":
			self.eventManager.add_event(random.choice([self.eventManager.event_good_harvest, self.eventManager.event_bad_harvest]))
			self.season = "Wind"
		elif self.season == "Wind":
			self.season = "Earth"
			self.year += 1

	def random_event(self):
		possibleEvents = []
		possibleEvents.append(self.eventManager.event_nothing)
		if self.season == "Wind":
			if random.randint(1, 100) <= 25:
				possibleEvents.append(self.eventManager.event_blizzard)
		self.eventManager.run_event(random.choice(possibleEvents))

	def feed_villagers(self):
		if self.food.amount < len(self.villagers):
			self.eventManager.add_event(self.eventManager.event_starvation)
		else:
			self.food.amount -= len(self.villagers)

	def update_pregnancy(self):
		for villager in self.villagers:
			if villager.gender == "female" and villager.isPregnant:
				villager.pregnantSeasons += 1
				if villager.pregnantSeasons > 3:
					newborn = Villager()
					newborn.age = 0
					newborn.birthSeason = self.season
					self.villagers.append(newborn)
					villager.isPregnant = False
					villager.pregnantSeasons = 0
			if villager.gender == "female" and not villager.isPregnant:
				if 15 <= villager.age < 40:
					if self.get_adults("male") > 0:
						if random.randint(1, 100) <= 25:
							villager.isPregnant = True

	def get_adults(self, gender="both"):
		amount = 0
		for villager in self.villagers:
			if villager.age >= 15:
				if gender == "both":
					amount += 1
				elif villager.gender == gender:
					amount += 1
		return amount

	def get_children(self, gender="both"):
		amount = 0
		for villager in self.villagers:
			if villager.age < 15:
				if gender == "both":
					amount += 1
				elif villager.gender == gender:
					amount += 1
		return amount

game = Game()
game.run()
