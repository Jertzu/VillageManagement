import random

from classes import Villager

class EventManager():
	def __init__(self, game):
		self.game = game
		self.eventList = []
		self.currentEvent = None

	def add_event(self, event):
		self.eventList.append(event)

	def run_event(self, event):
		self.currentEvent = None
		self.game.pendingActions = "next_event"
		self.eventList.insert(0, event)

	def event_nothing(self):
		event = Event(self.game, "It was a peaceful season.")
		event.choices.append(EventChoice("[ENTER] Continue.", "ENTER", None))
		self.currentEvent = event

	def event_beginning(self):
		event = Event(self.game, "You led a small group of people to settle unknown lands. After finding a promising piece of land, you quickly build a small village.")
		event.choices.append(EventChoice("[ENTER] Continue.", "ENTER", None))
		self.currentEvent = event

	def event_good_harvest(self):
		event = Event(self.game, "This years harvest was good.")
		event.choices.append(EventChoice("[ENTER] Continue.", "ENTER", None))

		self.game.food.amount += int(self.game.get_adults() * 5)

		self.currentEvent = event

	def event_bad_harvest(self):
		event = Event(self.game, "This years harvest was bad.")
		event.choices.append(EventChoice("[ENTER] Continue.", "ENTER", None))
		event.choices.append(EventChoice("[A] Run blizzard event.", "a", self.event_blizzard))

		self.game.food.amount += int(self.game.get_adults() * 3)

		self.currentEvent = event

	def event_starvation(self):
		starvedVillagers = []
		for villager in self.game.villagers:
			if self.game.food.amount > 0:
				self.game.food.amount -= 1
				villager.starvation = 0
			else:
				villager.starvation += 1

			if villager.starvation == 1:
				if random.randint(1, 100) <= 50:
					starvedVillagers.append(villager)
			elif villager.starvation > 1:
				if random.randint(1, 100) <= 75:
					starvedVillagers.append(villager)

		for dead in starvedVillagers:
			self.game.villagers.remove(dead)

		event = Event(self.game, "You didn't have enough food to feed everyone!")
		if len(starvedVillagers) > 0:
			event.eventText += " " + str(len(starvedVillagers)) + " villagers starved to death!"
		event.choices.append(EventChoice("[ENTER] Continue.", "ENTER", None))
		self.currentEvent = event

	def event_blizzard(self):
		deaths = random.randint(0, int(len(self.game.villagers) / 4))
		event = Event(self.game, "There was a huge blizzard. " + str(deaths) + " villagers died.")
		event.choices.append(EventChoice("[ENTER] Continue.", "ENTER", None))
		for i in range(deaths):
			if len(self.game.villagers) > 0:
				self.game.villagers.pop(random.randint(0, int(len(self.game.villagers) - 1)))
		self.currentEvent = event

	def handle_input(self, input):
		for choice in self.currentEvent.choices:
			if choice.key == input.key or choice.key == input.keychar:
				if choice.event:
					self.run_event(choice.event)
				else:
					self.currentEvent = None


class Event():
	def __init__(self, game, eventText):
		self.game = game
		self.eventText = eventText
		self.choices = []

	def draw(self):
		textY = 3
		self.game.console.draw_str(3, textY, self.eventText)
		textY += 2
		for choice in self.choices:
			self.game.console.draw_str(3, textY, choice.choiceText)
			textY += 1


class EventChoice():
	def __init__(self, choiceText, key, event):
		self.choiceText = choiceText
		self.key = key
		self.event = event