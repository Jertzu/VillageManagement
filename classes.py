import random


class Villager():
	def __init__(self):
		self.age = random.randint(15, 40)
		self.gender = random.choice(["male", "female"])
		self.birthSeason = random.choice(["Earth", "Fire", "Water", "Wind"])
		self.starvation = 0
		if self.gender == "female":
			self.isPregnant = False
			self.pregnantSeasons = 0

class Resource():
	def __init__(self, name, amount):
		self.name = name
		self.amount = amount